# Information System 2018

## Installation

- `git clone https://gitlab.com/SergoFan/information-system-2018.git`
- `yarn` / `npm install`

## Usage

### Development

```bash
# build
npm run dev
```

```bash
# build and watch
npm run watch

# serve with hot reloading
npm run hot
```

### Production

```bash
npm run production
```