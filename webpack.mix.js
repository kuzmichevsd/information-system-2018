const mix = require('laravel-mix');
const path = require('path');

mix.config.vue.esModule = true;

mix
    .disableNotifications()
    .options({
        extractVueStyles: false,
        uglify: {
            uglifyOptions: {
                compress: {
                    drop_console: true,
                }
            }
        }
    })
    .setPublicPath('')
    .js('src/js/app.js', 'dist')
    .sass('src/sass/app.scss', 'dist')
    .sourceMaps();

if (mix.inProduction()) {
    mix.version();
}

mix.browserSync({
    proxy: 'localhost',
    notify: false,
    open: false,
});

mix.webpackConfig({
    plugins: [],
    resolve: {
        extensions: ['.js', '.json', '.vue'],
        alias: {
            '~': path.join(__dirname, './src/js')
        }
    },
    output: {
        chunkFilename: 'dist/[name].js?id=[chunkhash]',
        publicPath: mix.config.hmr ? '//localhost:8080' : '/'
    }
});